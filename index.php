<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");

echo "Name : $sheep->name"; // "shaun"
echo "<br>";
echo "legs : $sheep->legs"; // 4
echo "<br>";
echo "cold blooded : $sheep->cold_blooded"; // "no"
echo "<br>";

echo "<br>";

$frog = new Frog('buduk');
echo "Name : $frog->name";
echo "<br>";
echo "legs : $frog->legs";
echo "<br>";
echo "cold blooded : $frog->cold_blooded";
echo "<br>";
echo "Jump : ";
$frog->jump();
echo "<br>";

echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name : $sungokong->name";
echo "<br>";
echo "legs : $sungokong->legs";
echo "<br>";
echo "cold blooded : $sungokong->cold_blooded";
echo "<br>";
echo "Yell : ";
$sungokong->yell();
echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())